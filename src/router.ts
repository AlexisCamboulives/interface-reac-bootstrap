import { Application } from "express";
import CritereController from "./controllers/CritereController";
import CompetenceController from "./controllers/CompetenceController";
import HomeController from "./controllers/HomeController";
import LoginController from "./controllers/LoginController";
import RegisterController from "./controllers/RegisterController";
import {redirectUser} from "./server";
import { Request, ParamsDictionary, Response } from "express-serve-static-core";
import { ParsedQs } from "qs";

export default function route(app: Application)
{
    /** Static pages **/
    app.get('/', (req, res) =>
    {
        HomeController.index(req, res);
    });

    app.get('/competence', (req, res) =>
    {   
        CompetenceController.competence(req, res);
    });

    app.get('/critere/:id',redirectUser, (req: Request<ParamsDictionary, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>, number>, next: any) =>
    {
        req.params.id
        CritereController.critere(req, res);
    });

    app.post('/critere/:id', (req, res) =>
    {
        req.params.id
        CritereController.checkbox(req, res);
    });
    app.get('/login', (req, res) =>
    {
        LoginController.showFormLog(req, res);
    });
    app.post('/login', (req, res) =>
    {
        LoginController.login(req, res);
    });
    app.get('/login', (req, res) =>
    {
        LoginController.showFormLogUserError(req, res);
    });
    app.get('/login', (req, res) => 
    {
        LoginController.showFormLogPasswordError(req, res);
    });
    app.get('/register', (req, res) =>
    {
        RegisterController.showFormReg(req, res);
    });
    app.post('/register', (req, res) =>
    {
        RegisterController.enregister(req, res);
    });
    app.get('/register', (req, res)=>
    {
        RegisterController.showFormRegEchec(req, res);
    });
}

function showFormLog(arg0: string, showFormLog: any, arg2: (req: import("express-serve-static-core").Request<{ id: string; }, any, any, import("qs").ParsedQs, Record<string, any>>, res: import("express-serve-static-core").Response<any, Record<string, any>, number>, next: import("express-serve-static-core").NextFunction) => void) {
    throw new Error("Function not implemented.");
}


