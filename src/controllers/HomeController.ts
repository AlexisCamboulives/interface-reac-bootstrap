import { Request, Response } from "express-serve-static-core";
import { allowedNodeEnvironmentFlags } from "process";
//import { smdb } from "../server";
import { db } from "../server";

export default class HomeController
{
    static index(req: Request, res: Response): void
    {
        res.render('pages/index', {
            title: 'Bienvenue',
            active1: 'active'
        });
    }
}

