import { Request, Response } from "express-serve-static-core";

import { db } from "../server";
import CompetenceController from "./CompetenceController";
import LoginController from "./LoginController";

export default class CritereController {
    static checkbox(req: Request, res: Response): void {
        console.log(req.session)
        //@ts-ignore
        const myUser = req.session.user
        let recevu = req.body.checkbox;
        let pivottbl = db.prepare('SELECT criteres_id FROM user_has_criteres WHERE user_id = ?').all(myUser);
        pivottbl = pivottbl.map(e => Number(e.criteres_id));
        console.log('myUser:' +myUser);

        const id = req.params.id;
        const critComp1db = db.prepare('SELECT * FROM criteres WHERE competences_id = ?').all(id);

        let critCompID = critComp1db.map(e => Number(e.id));

        if (recevu == null) {
            
            
            console.log(req.body)
            const nul = db.prepare('DELETE FROM user_has_criteres WHERE user_id = ?').run(myUser)
        }
            else if (typeof(recevu)=='object'){
                recevu = recevu.map((e: any) => Number(e));
    
                critComp1db.forEach(element => {
                    if (!pivottbl.includes(element.id) && recevu.includes(element.id)) {
                        const hope = db.prepare('INSERT INTO user_has_criteres ("user_id", "criteres_id") VALUES (?, ?)').run(myUser, element.id);
                    }
                    else if (pivottbl.includes(element.id) && !recevu.includes(element.id)) {
                        const wiper = db.prepare('DELETE FROM user_has_criteres WHERE criteres_id = ? AND user_id = ? ').run(element.id, myUser)
                    }
                });
            }
            else if (typeof Number(recevu) == 'number') {

            const addsolo = db.prepare('INSERT INTO user_has_criteres ("user_id", "criteres_id") VALUES (?, ?)').run(myUser, recevu);

            for (let i = 0; i < critCompID.length; i++) {
                if (pivottbl.includes(critCompID[i]) && recevu != critCompID[i]) {
                    const nulll = db.prepare('DELETE FROM user_has_criteres WHERE user_id = ? AND criteres_id = ?').run(myUser, critCompID[i])
                }
            }
        }

        CompetenceController.competence(req, res)
    }

    /**
     * Show criteres form
     * @param req 
     * @param res 
     */
    static critere(req: Request, res: Response): void {
         //@ts-ignore
        const myUser = req.session.user;
        const firstUser = db.prepare('SELECT * FROM user WHERE id = ?').all(myUser);
        
        const id = req.params.id;
        const critereFromCompetenceId = db.prepare('SELECT * FROM criteres WHERE competences_id = ?').all(id);

        let checker = db.prepare('SELECT criteres_id FROM user_has_criteres WHERE user_id = ?').all(myUser);
        checker = checker.map(e => Number(e.criteres_id));
        //console.log(checker)
        critereFromCompetenceId.forEach(critere => {
            //console.log(critere.id)
            if (checker.includes(critere.id)) {
                critere.checked = "checked";
            }
            else {
                critere.checked = "";
            }
        })
        //console.log(critComp1db)
        res.render('pages/critere', {
            title: 'Critères',
            critere: critereFromCompetenceId,
            index: id
        });
    }
}