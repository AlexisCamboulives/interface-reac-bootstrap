import { Request, Response } from "express-serve-static-core";
import { db } from "../server";
import CompetenceController from "./CompetenceController";
import HomeController from "./HomeController";
import bcrypt from 'bcrypt';

export default class LoginController {
    static showFormLogUserError //     });
        (req: Request<{}, any, any, import("qs").ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>, number>) {
        throw new Error("Method not implemented.");
    }
    static showFormLogPasswordError(req: Request<{}, any, any, import("qs").ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>, number>) {
        throw new Error("Method not implemented.");
    }
    static showFormLog(req: Request, res: Response): void {
        console.log(req.session);
        res.render('pages/login', {
            title: "Se connecter",
            active3: 'active'
        });
    }
    static login(req: Request, res: Response): void {
        console.log(req.body.username);
        const identifiant = db.prepare("SELECT * FROM user WHERE username=?").get(req.body.username);
        console.log(identifiant);
        const hash = identifiant.password;

        if (identifiant) {
            const compare = bcrypt.compareSync(req.body.password, hash);
            if (compare) {
                //@ts-ignore
                req.session.user = identifiant.id;
                CompetenceController.competence(req, res);
                console.log(req.session);
            }
            else{
                LoginController.showFormLog(req, res);
            }

        }
        else {
            LoginController.showFormLog(req, res);
        }


    }

}

