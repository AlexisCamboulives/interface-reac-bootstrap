import { Request, Response } from "express-serve-static-core";
import { db } from "../server";
import bcrypt from 'bcrypt';

export default class RegisterController {
    static showFormReg(req: Request, res: Response): void {

        res.render('pages/register', {
            title: "S'inscrire",


        });
    }
    static enregister(req: Request, res: Response): void {
        let nameOfUsers = db.prepare('SELECT ("username") FROM user').all()
        nameOfUsers = nameOfUsers.map(e => String(e.username));
        let userName = req.body.username;
        let passWord = req.body.password;
        if (!nameOfUsers.includes(userName)) {
            const saltRounds = 10;
            bcrypt.hash(passWord, saltRounds, function(err, hash) {
                db.prepare('INSERT INTO user ("username", "password") VALUES (?, ?)').run(userName, hash);
                console.log(hash);
            });
            
        }
        else {

            RegisterController.showFormRegEchec(req, res);
        }
    }
    static showFormRegEchec(req: Request, res: Response): void {
        res.render('pages/register', {
            title: "S'inscrire",
            messageCreation: "Ce nom est déjà utilisé !!!!!!!"

        });
    }
}
