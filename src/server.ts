// Import dependencies
import path from "path";
import fs from 'fs';
import express from 'express';
import bodyParser from 'body-parser';
import Database from 'better-sqlite3';
const { config, engine } = require('express-edge');
import session from 'express-session';
import cookieSession from 'cookie-session';
import cookieParser from "cookie-parser";


// Import router
import router from './router';

// Initialize the express engine
const app: express.Application = express();

// Take a port 3000 for running server.
const port: number = 3000;

export const db = new Database(path.join(__dirname, './database/data.db'), { verbose: () => { } });
//export const smdb = db.prepare('SELECT * FROM competences').all();

/**
 * creation de la base de données, et insertion des données
 * A ne lancer qu'une seule fois
 */

    //  const migration = fs.readFileSync(path.join(__dirname, '../data-struture.sql'), 'utf8');
    //  db.exec(migration);
    // const migrationData = fs.readFileSync(path.join(__dirname, '../data.sql'), 'utf8');
    //  db.exec(migrationData);
    

    



export const redirectUser = (req: Request, res: any, next:any) => {
    //@ts-ignore
    if(!req.session.user){
        res.redirect('/login')
    }
    else{
        next();
    }
}

app.locals.db = db;

const oneDay = 1000*60*60*24;
app.use(session({
    secret: "thisismysecrctekeyfhrgfgrfrty84fwir767",
    saveUninitialized:true,
    cookie: { maxAge: oneDay, signed: true},
    resave: false
}));

app.use(cookieParser());

// define the templating engine
app.use(engine);


// define the Views folder
app.set("views", path.join(__dirname, "./views"));

app.use(
    express.static(path.join(__dirname, "public"), { maxAge: 31557600000 })
);

// to read request form body
app.use(bodyParser.urlencoded({ extended: true }));



// Routes
router(app);

// Server setup
app.listen(port, () =>
{
    console.log(`TypeScript with Express http://localhost:${port}/`);
});

